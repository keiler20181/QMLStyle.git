import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Controls.Universal 2.15

ApplicationWindow  {
    id:root
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")


    Column{
        anchors.fill: parent
        Button{

        }

        Slider{

        }

        CheckBox{
        }

        TextEdit{
            text: "test"
        }

        RadioButton{
        }
        RadioButton{
        }

        ComboBox{
            model: [ "1", "2", "3","4" ]
        }

        TextField{
            text: "test"
        }

        TextInput{}
    }
}
